# Frontend Mentor - Time tracking dashboard

This is a solution to the [Time tracking dashboard](https://www.frontendmentor.io/challenges/time-tracking-dashboard-UIQ7167Jw). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page
- Switch between viewing Daily, Weekly, and Monthly stats

### Screenshot

![](./screenshot.jpg)

### Links

- Solution URL: [https://bitbucket.org/nmoraja/time-tracking-dashboard/src](https://bitbucket.org/nmoraja/time-tracking-dashboard/src)
- Live Site URL: [https://nmorajda-time-tracking-dashboard.netlify.app/](https://nmorajda-time-tracking-dashboard.netlify.app/)

## My process

### Built with

**Compilation .pug to .html using VSC extension: Live Pug Compiler.**

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- JavaScript ES6

## Author

- Website - [N. Morajda](https://abmstudio.pl)
- Frontend Mentor - [@nmorajda](https://www.frontendmentor.io/profile/nmorajda)
- Github - [nmorajda](https://github.com/nmorajda)
- Bitbucket - [nmorajda](https://bitbucket.org/nmoraja/)
